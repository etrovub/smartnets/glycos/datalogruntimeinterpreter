#![feature(trait_alias)]
#![allow(unused_variables)]
#![allow(dead_code)]

#[macro_use]
extern crate lalrpop_util;
pub mod parsing;
pub mod reasoning;

type ID = usize;
