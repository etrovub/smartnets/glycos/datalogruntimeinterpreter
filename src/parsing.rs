use lalrpop_util::lexer::Token;
use lalrpop_util::ParseError;

pub trait Data = Eq + PartialEq + Clone;

/// Var is the representation of Variables
/// T is the representation of Atoms (data)

/// ## Rule
/// ### RuleHead
#[derive(Debug)]
pub struct RuleHead<Var: Data, T: Data> {
    pub predicate: T,
    pub arguments: Vec<Argument<Var, T>>,
}

/// ### Subgoal
#[derive(Debug, Clone)]
pub enum Subgoal<Var: Data, T: Data> {
    Logic(LogicSubgoal<Var, T>),
    Arithmetic(ArithmeticSubgoal<Var, T>),
}
#[derive(Debug, Clone)]
pub struct LogicSubgoal<Var: Data, T: Data> {
    pub predicate: T,
    pub arguments: Vec<Argument<Var, T>>,
}

#[derive(Debug, Clone)]
pub struct ArithmeticSubgoal<Var: Data, T: Data> {
    pub lhs: Argument<Var, T>,
    pub operator: ComparisonOperator,
    pub rhs: Argument<Var, T>,
}

/// ### Rule
#[derive(Debug)]
pub struct Rule<Var: Data, T: Data> {
    pub head: RuleHead<Var, T>,
    pub subgoals: Vec<Subgoal<Var, T>>,
}

/// ### Question
#[derive(Clone)]
pub struct Question<Var: Data, T: Data> {
    pub predicate: T,
    pub arguments: Vec<Argument<Var, T>>,
}

impl<Var: Data, T: Data> Question<Var, T> {
    fn from(fact: Fact<Var, T>) -> Self {
        Self {
            predicate: fact.predicate,
            arguments: fact.arguments,
        }
    }
}

/// ## Clause
pub enum Clause<Var: Data, T: Data> {
    Fact(Fact<Var, T>),
    Rule(Rule<Var, T>),
    Question(Question<Var, T>),
}

/// ## Variables
#[derive(Debug, Eq, PartialEq, Clone)]
pub struct Variable<Var: Data> {
    pub name: Var,
}

/// ## Atoms
#[derive(Debug, Eq, PartialEq, Clone)]
pub struct Atom<T: Data> {
    pub name: T,
}

/// ## Argument
#[derive(Debug, Eq, PartialEq, Clone)]
pub enum Argument<Var: Data, T: Data> {
    Atom(Atom<T>),
    Variable(Variable<Var>),
    Number(u64), //todo remove?
}

/// ## Fact
pub struct Fact<Var: Data, T: Data> {
    pub predicate: T,
    pub arguments: Vec<Argument<Var, T>>,
}

/// ## Arithmetic
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum ComparisonOperator {
    Lt,
    Gt,
    Lte,
    Gte,
}

// # Parsing
lalrpop_mod!(#[allow(clippy::all)] pub datalog);

pub struct Parser {}

impl Parser {
    pub fn parse(
        string: &str,
    ) -> Result<Clause<String, String>, ParseError<usize, Token<'_>, &'static str>> {
        datalog::DatalogParser::new().parse(string)
    }

    pub fn parse_ruleset(
        string: &str,
        ) -> Result<Vec<Rule<String, String>>, ParseError<usize, Token<'_>, &'static str>> {
        datalog::RulesetParser::new().parse(string)
    }

    pub fn parse_assertionset(
        string: &str,
        ) -> Result<Vec<Fact<String, String>>, ParseError<usize, Token<'_>, &'static str>> {
        datalog::AssertionsetParser::new().parse(string)
    }
}

/// # Tests
#[cfg(test)]
mod tests {
    use crate::parsing::{Argument, Atom, Clause, ComparisonOperator, Parser, Subgoal, Variable};
    use itertools::Itertools;

    #[test]
    fn test_fact1() {
        if let Clause::Fact(res) = Parser::parse("lie(cake).").unwrap() {
            assert_eq!(res.predicate, "lie");
            assert_eq!(
                res.arguments
                    .iter()
                    .filter_map(|arg| match arg {
                        Argument::Atom(atom) => Some(atom),
                        _ => None,
                    })
                    .map(|atom| atom.name.clone())
                    .collect::<Vec<String>>(),
                vec!("cake")
            )
        } else {
            panic!("Statement not parsed as a fact");
        }
    }

    #[test]
    fn test_fact2() {
        if let Clause::Fact(res) = Parser::parse("hasFriend(alice,bob).").unwrap() {
            assert_eq!(res.predicate, "hasFriend");
            assert_eq!(
                res.arguments
                    .iter()
                    .filter_map(|arg| match arg {
                        Argument::Atom(atom) => Some(atom),
                        _ => None,
                    })
                    .map(|atom| atom.name.clone())
                    .collect::<Vec<String>>(),
                vec!("alice", "bob")
            )
        } else {
            panic!("Statement not parsed as a fact");
        }
    }

    #[test]
    fn test_rule1() {
        if let Clause::Rule(res) =
            Parser::parse("canWrite(bob, wall) :- hasWall(alice, wall), hasFriend(alice, bob).")
                .unwrap()
        {
            assert_eq!(res.head.predicate, "canWrite");
            assert_eq!(
                res.head
                    .arguments
                    .iter()
                    .filter_map(|arg| match arg {
                        Argument::Atom(atom) => Some(atom),
                        _ => None,
                    })
                    .map(|x| x.name.clone())
                    .collect_vec(),
                vec!["bob", "wall"]
            );
            let logic_subgoals = res
                .subgoals
                .iter()
                .map(|x| match x {
                    Subgoal::Logic(l) => l,
                    Subgoal::Arithmetic(_) => panic!(),
                })
                .collect_vec();
            assert_eq!(
                logic_subgoals
                    .iter()
                    .map(|x| x.predicate.clone())
                    .collect_vec(),
                vec!["hasWall", "hasFriend"]
            );
            assert_eq!(
                logic_subgoals[0]
                    .arguments
                    .iter()
                    .filter_map(|arg| match arg {
                        Argument::Atom(atom) => Some(atom),
                        _ => None,
                    })
                    .map(|x| x.name.clone())
                    .collect_vec(),
                vec!["alice", "wall"]
            );
            assert_eq!(
                logic_subgoals[1]
                    .arguments
                    .iter()
                    .filter_map(|arg| match arg {
                        Argument::Atom(atom) => Some(atom),
                        _ => None,
                    })
                    .map(|x| x.name.clone())
                    .collect_vec(),
                vec!["alice", "bob"]
            );
            assert_eq!(logic_subgoals.len(), 2);
        } else {
            panic!("Statement not parsed as a rule")
        }
    }

    #[test]
    fn test_rule1_multiline() {
        if let Clause::Rule(res) =
            Parser::parse("canWrite(bob, wall) :- hasWall(alice, wall),\n
                                                  hasFriend(alice, bob).")
                .unwrap()
        {
            assert_eq!(res.head.predicate, "canWrite");
            assert_eq!(
                res.head
                    .arguments
                    .iter()
                    .filter_map(|arg| match arg {
                        Argument::Atom(atom) => Some(atom),
                        _ => None,
                    })
                    .map(|x| x.name.clone())
                    .collect_vec(),
                vec!["bob", "wall"]
            );
            let logic_subgoals = res
                .subgoals
                .iter()
                .map(|x| match x {
                    Subgoal::Logic(l) => l,
                    Subgoal::Arithmetic(_) => panic!(),
                })
                .collect_vec();
            assert_eq!(
                logic_subgoals
                    .iter()
                    .map(|x| x.predicate.clone())
                    .collect_vec(),
                vec!["hasWall", "hasFriend"]
            );
            assert_eq!(
                logic_subgoals[0]
                    .arguments
                    .iter()
                    .filter_map(|arg| match arg {
                        Argument::Atom(atom) => Some(atom),
                        _ => None,
                    })
                    .map(|x| x.name.clone())
                    .collect_vec(),
                vec!["alice", "wall"]
            );
            assert_eq!(
                logic_subgoals[1]
                    .arguments
                    .iter()
                    .filter_map(|arg| match arg {
                        Argument::Atom(atom) => Some(atom),
                        _ => None,
                    })
                    .map(|x| x.name.clone())
                    .collect_vec(),
                vec!["alice", "bob"]
            );
            assert_eq!(logic_subgoals.len(), 2);
        } else {
            panic!("Statement not parsed as a rule")
        }
    }

    #[test]
    fn test_multiple_rules() {
        //TODO: test more stuff
        let rules = Parser::parse_ruleset("ruleOne(Test) :- ruleTwo(Test).\nruleThree(test) :- ruleFour(foo).").unwrap();
        assert_eq!(rules[0].head.predicate, "ruleOne");
        assert_eq!(rules[0].head.arguments[0], Argument::Variable(Variable {name: "Test".to_string()}));
        assert_eq!(rules[1].head.predicate, "ruleThree");
        assert_eq!(rules[1].head.arguments[0], Argument::Atom(Atom {name: "test".to_string()}));

    }

    #[test]
    fn test_rule_with_variables() {
        if let Clause::Rule(res) =
            Parser::parse("canWrite(Bob, Wall) :- hasWall(Alice, Wall), hasFriend(Alice, Bob).")
                .unwrap()
        {
            assert_eq!(res.head.predicate, "canWrite");
            assert_eq!(
                res.head
                    .arguments
                    .iter()
                    .filter_map(|arg| match arg {
                        Argument::Variable(variable) => Some(variable),
                        _ => None,
                    })
                    .map(|x| x.name.clone())
                    .collect_vec(),
                vec!["Bob", "Wall"]
            );
            let logic_subgoals = res
                .subgoals
                .iter()
                .map(|x| match x {
                    Subgoal::Logic(l) => l,
                    Subgoal::Arithmetic(_) => panic!(),
                })
                .collect_vec();
            assert_eq!(
                logic_subgoals
                    .iter()
                    .map(|x| x.predicate.clone())
                    .collect_vec(),
                vec!["hasWall", "hasFriend"]
            );
            assert_eq!(
                logic_subgoals[0]
                    .arguments
                    .iter()
                    .filter_map(|arg| match arg {
                        Argument::Variable(var) => Some(var),
                        _ => None,
                    })
                    .map(|x| x.name.clone())
                    .collect_vec(),
                vec!["Alice", "Wall"]
            );
            assert_eq!(
                logic_subgoals[1]
                    .arguments
                    .iter()
                    .filter_map(|arg| match arg {
                        Argument::Variable(var) => Some(var),
                        _ => None,
                    })
                    .map(|x| x.name.clone())
                    .collect_vec(),
                vec!["Alice", "Bob"]
            );
            assert_eq!(logic_subgoals.len(), 2);
        } else {
            panic!("Statement not parsed as a rule")
        }
    }

    #[test]
    fn test_rule_with_atoms_and_variables() {
        if let Clause::Rule(res) =
            Parser::parse("bobsFriend(Alice) :- hasFriend(bob, Alice).").unwrap()
        {
            assert_eq!(res.head.predicate, "bobsFriend");
            assert_eq!(
                res.head.arguments[0],
                Argument::Variable(Variable {
                    name: "Alice".to_string()
                })
            );
            match &res.subgoals[0] {
                Subgoal::Logic(l) => {
                    assert_eq!(l.predicate, "hasFriend");
                    assert_eq!(
                        l.arguments[0],
                        Argument::Atom(Atom {
                            name: "bob".to_string()
                        })
                    );
                    assert_eq!(
                        l.arguments[1],
                        Argument::Variable(Variable {
                            name: "Alice".to_string()
                        })
                    );
                }
                _ => panic!(),
            }
        }
    }

    #[test]
    fn test_rule_with_arithmetic() {
        if let Clause::Rule(res) =
            Parser::parse("over18(person) :- hasAge(person, age), age >= 18.").unwrap()
        {
            assert_eq!(res.head.predicate, "over18");
            assert_eq!(
                res.head
                    .arguments
                    .iter()
                    .filter_map(|arg| match arg {
                        Argument::Atom(atom) => Some(atom),
                        _ => None,
                    })
                    .map(|x| x.name.clone())
                    .collect_vec(),
                vec!["person"]
            );
            match &res.subgoals[0] {
                Subgoal::Logic(l) => assert_eq!(
                    l.arguments
                        .iter()
                        .filter_map(|arg| match arg {
                            Argument::Atom(atom) => Some(atom),
                            _ => None,
                        })
                        .map(|x| x.name.clone())
                        .collect_vec(),
                    vec!["person", "age"]
                ),
                Subgoal::Arithmetic(_) => panic!("Parsed as arithmetic instead of logic"),
            }
            match &res.subgoals[1] {
                Subgoal::Logic(_) => panic!("Parsed as logic instead of arithmetic"),
                Subgoal::Arithmetic(a) => {
                    assert_eq!(
                        a.lhs,
                        Argument::Atom(Atom {
                            name: "age".to_string()
                        })
                    );
                    assert_eq!(a.rhs, Argument::Number(18));
                    assert_eq!(a.operator, ComparisonOperator::Gte);
                }
            }
        } else {
            panic!()
        }
    }

    #[test]
    fn test_question() {
        if let Clause::Question(res) = Parser::parse("?- hasFriend(alice,bob).").unwrap() {
            assert_eq!(res.predicate, "hasFriend");
            assert_eq!(
                res.arguments
                    .iter()
                    .filter_map(|arg| match arg {
                        Argument::Atom(atom) => Some(atom),
                        _ => None,
                    })
                    .map(|atom| atom.name.clone())
                    .collect::<Vec<String>>(),
                vec!("alice", "bob")
            )
        } else {
            panic!("Statement not parsed as a question");
        }
    }
}
