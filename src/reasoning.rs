use crate::parsing::{
    ArithmeticSubgoal, Argument, Atom, ComparisonOperator, Data, Fact, LogicSubgoal, Question, RuleHead, Subgoal,
    Variable,
};
use itertools::Itertools;
use std::collections::HashMap;
use std::fmt::{Debug, Formatter};
use std::hash::Hash;
use std::rc::Rc;

/// # Data Types for assertions
#[derive(Clone, PartialEq, Eq)]
pub enum DataType<T: Data> {
    Atom(Atom<T>),
    Number(u64),
}

/// # Assertion set representation
#[derive(Clone)]
pub struct Assertion<T: Data> {
    pub parents: Option<Vec<Rc<Assertion<T>>>>,
    pub predicate: T,
    pub arguments: Vec<DataType<T>>,
}

impl<T: Data> Assertion<T> {
    pub fn new<Var: Data>(
        parents: Option<Vec<Rc<Assertion<T>>>>,
        predicate: &T,
        arguments: Vec<Argument<Var, T>>,
    ) -> Self {
        Self {
            parents,
            predicate: predicate.clone(),
            arguments: arguments
                .into_iter()
                .map(|arg| match arg {
                    Argument::Atom(atm) => DataType::Atom(atm),
                    Argument::Variable(_) => panic!("Assertions can't contain Variables"),
                    Argument::Number(nr) => DataType::Number(nr),
                })
                .collect_vec(),
        }
    }

    pub fn from_datatypes(
        parents: Option<Vec<Rc<Assertion<T>>>>,
        predicate: &T,
        arguments: Vec<DataType<T>>,
    ) -> Self {
        Self {
            parents,
            predicate: predicate.clone(),
            arguments,
        }
    }
}

impl<T: Data> PartialEq for Assertion<T> {
    fn eq(&self, other: &Self) -> bool {
        (self.predicate == other.predicate) && (self.arguments == other.arguments)
    }
}
impl<T: Data> Eq for Assertion<T> {}

impl<T: Data> Assertion<T> {
    pub fn from_logic_goal<Var: Data>(
        head: &LogicSubgoal<Var, T>,
        parents: Vec<Rc<Assertion<T>>>,
        values: Vec<Argument<Var, T>>,
    ) -> Self {
        Assertion::new(Some(parents), &head.predicate, values)
    }

    pub fn get_history(&self) -> History<T> {
        let mut iterations = Vec::new();
        let mut roots = vec![self.clone()];
        let mut leafs = Vec::new();
        while !roots.is_empty() {
            let mut iteration = Iteration{assertions: Vec::new()};
            let mut new_roots = Vec::new();
            for root in roots {
                iteration.assertions.push(root.clone());
                if let Some(parents) = root.parents {
                    new_roots.extend(parents.iter().map(|p| p.deref().clone()));
                } else {
                    leafs.push(root.clone());
                }
            }
            iterations.push(iteration);
            roots = new_roots;
        }
        History::new(iterations.into_iter().rev().collect(), leafs)
    }
}

impl<V: Data> Assertion<V> {
    pub fn from_fact(fact: &Fact<String, V>) -> Self {
        Assertion::new(None, &fact.predicate, fact.arguments.clone())
    }
}

struct PredicateBucketMap<K: Data + Hash, V: Clone + Eq> {
    buckets: HashMap<K, Vec<V>>,
}

impl<K: Data + Hash, V: Clone + Eq> Default for PredicateBucketMap<K, V> {
    fn default() -> Self {
        PredicateBucketMap {
            buckets: HashMap::new(),
        }
    }
}

impl<K: Data + Hash, V: Clone + Eq> PredicateBucketMap<K, V> {
    pub fn get(&self, key: &K) -> Option<&Vec<V>> {
        self.buckets.get(key)
    }

    pub fn insert(&mut self, key: K, val: V) -> bool {
        let entry = self
            .buckets
            .entry(key)
            .or_insert_with(|| Vec::with_capacity(1));
        if entry.contains(&val) {
            false
        } else {
            entry.push(val);
            true
        }
    }
}

struct AssertionSet<T: Data + Hash> {
    predicates: PredicateBucketMap<T, Rc<Assertion<T>>>,
}

impl<T: Data + Hash> Default for AssertionSet<T> {
    fn default() -> Self {
        Self {
            predicates: PredicateBucketMap::default(),
        }
    }
}

impl<T: Data + Hash> AssertionSet<T> {
    pub fn get(&self, key: &T) -> Option<&Vec<Rc<Assertion<T>>>> {
        self.predicates.get(key)
    }
    pub fn insert(&mut self, key: T, val: Assertion<T>) -> bool {
        self.predicates.insert(key, Rc::new(val))
    }

    pub fn contains(&self, key: T, val: &Assertion<T>) -> bool {
        match self.get(&key) {
            Some(assertions) => assertions.iter().any(|a| **a == *val),
            None => false,
        }
    }

    pub fn find(&self, key: &T, val: &Assertion<T>) -> Option<Rc<Assertion<T>>> {
        match self.get(&key) {
            Some(assertions) => assertions.iter().find(|a| ***a == *val).cloned(),
            None => None,
        }
    }
}
/// ## Question
impl<Var: Data, T: Data> Question<Var, T> {
    fn to_assertion(&self) -> Assertion<T> {
        Assertion::new(None, &self.predicate, self.arguments.clone())
    }
}

#[derive(Ord, PartialOrd, PartialEq, Eq, Clone)]
pub enum UnificationLocation {
    Head(usize),
    LogicSubgoal((usize, usize)),
    ArithmeticSubgoal((usize, usize)),
}

impl UnificationLocation {
    fn is_head(&self) -> bool {
        matches!(self, UnificationLocation::Head(_))
    }
}

#[derive(Clone)]
pub struct RuleUnification<Var: Data> {
    pub locations: Vec<(Variable<Var>, Vec<UnificationLocation>)>,
}

impl<Var: Data> Default for RuleUnification<Var> {
    fn default() -> Self {
        RuleUnification {
            locations: Vec::new(),
        }
    }
}

impl<Var: Data> RuleUnification<Var> {
    pub fn add_variable(&mut self, name: Variable<Var>, location: UnificationLocation) {
        let item = self.locations.iter_mut().find(|(d, _)| d == &name);
        match item {
            Some((_, v)) => v.push(location),
            None => self.locations.push((name.clone(), vec![location])),
        }
    }
}

pub struct Bindings<Var: Data, T: Data> {
    pub bindings: Vec<(Variable<Var>, DataType<T>)>,
}

impl<Var: Data, T: Data> Default for Bindings<Var, T> {
    fn default() -> Self {
        Bindings { bindings: vec![] }
    }
}

impl<Var: Data, T: Data> Bindings<Var, T> {
    pub fn new() -> Self {
        Self::default()
    }
    pub fn bind_or_get(&mut self, var: &Variable<Var>, data: &DataType<T>) -> Option<DataType<T>> {
        match self.bindings.iter().find(|(v, _)| var == v) {
            Some((_var, res)) => Some(res.clone()),
            None => {
                self.bindings.push((var.clone(), data.clone()));
                None
            }
        }
    }

    fn get(&self, var: &Variable<Var>) -> Option<DataType<T>> {
        self.bindings
            .iter()
            .find(|(v, _)| var == v)
            .map(|(_, data)| data.clone())
    }

    pub fn fill_in(
        &self,
        rule: &ReasonerRule<Var, T>,
        parents: &[Rc<Assertion<T>>],
    ) -> Assertion<T> {
        let datas: Vec<DataType<T>> = rule
            .head
            .arguments
            .iter()
            .map(|arg| match arg {
                Argument::Atom(atm) => DataType::Atom(atm.clone()),
                Argument::Variable(var) => {
                    if let Some(data) = self.get(var) {
                        data
                    } else {
                        panic!("Unbound variable");
                    }
                }
                Argument::Number(_) => panic!("Arithmetic filled in. Should this happen?"),
            })
            .collect_vec();
        Assertion::from_datatypes(Some(parents.to_vec()), &rule.head.predicate, datas)
    }
}

#[derive(Clone)]
pub struct ReasonerRule<Var: Data, T: Data> {
    pub head: LogicSubgoal<Var, T>, // TODO: Make this a RuleHead?
    pub logic_subgoals: Vec<LogicSubgoal<Var, T>>,
    pub arithmetic_subgoals: Vec<ArithmeticSubgoal<Var, T>>,
    pub indices: RuleUnification<Var>,
}

impl<Var: Data, T: Data> ReasonerRule<Var, T> {
    pub fn new(head: &RuleHead<Var, T>, subgoals: &[Subgoal<Var, T>]) -> Self {
        let mut indices = RuleUnification::default();
        for i in 0..head.arguments.len() {
            if let Argument::Variable(var) = &head.arguments[i] {
                indices.add_variable(var.clone(), UnificationLocation::Head(i))
            }
        }
        let mut logic_subgoals = vec!{};
        let mut arithmetic_subgoals = vec!{};
        for (i, item) in subgoals.iter().enumerate() {
            match item {
                Subgoal::Logic(subgoal) => {
                    for j in 0..subgoal.arguments.len() {
                        if let Argument::Variable(var) = &subgoal.arguments[j] {
                            indices.add_variable(var.clone(), UnificationLocation::LogicSubgoal((i, j)))
                        }
                    }
                    logic_subgoals.push(subgoal.clone());
                }
                //LHS location 0, RHS location 1
                Subgoal::Arithmetic(subgoal) => {
                    if let Argument::Variable(var) = &subgoal.lhs {
                        indices.add_variable(var.clone(), UnificationLocation::ArithmeticSubgoal((i, 0)))
                    } else if let Argument::Variable(var) = &subgoal.rhs {
                        indices.add_variable(var.clone(), UnificationLocation::ArithmeticSubgoal((i, 1)))
                    }
                    arithmetic_subgoals.push(subgoal.clone());
                }
            }
        }
        let head_subgoal = LogicSubgoal {
            predicate: head.predicate.clone(),
            arguments: head.arguments.clone(),
        };
        Self {
            head: head_subgoal,
            logic_subgoals,
            arithmetic_subgoals,
            indices,
        }
    }

    fn is_match(&self, assertions: &[Rc<Assertion<T>>]) -> Option<Bindings<Var, T>> {
        // We first try to match logic subgoals, in order to bind values to variables.
        // Afterwards, we check the arithmetic subgoals, as a filter.
        let mut bindings = Bindings::new();
        // Compare logic subgoals
        for (sg, asrt) in self.logic_subgoals.iter().zip(assertions.iter()) {
                    // Compare predicate
                    if sg.predicate != asrt.predicate {
                        return None;
                    }
                    // Compare arguments
                    if sg.arguments.len() != asrt.arguments.len() {
                        return None;
                    }
                    for (rule_arg, data) in sg.arguments.iter().zip(asrt.arguments.iter()) {
                        match rule_arg {
                            Argument::Variable(var) => {
                                // Try to bind, if bound compare result
                                match bindings.bind_or_get(var, data) {
                                    Some(val) if &val != data => return None,
                                    _ => {}
                                }
                            }
                            // Atoms match
                            Argument::Atom(ratm) => {
                                // Compare to data
                                match data {
                                    // Is atom with same value -> still matches
                                    DataType::Atom(atm) if ratm == atm => {}
                                    // Otherwise no match
                                    _ => return None,
                                }
                            }
                            // Atoms don't match
                            Argument::Number(rnr) => {
                                // Compare to data
                                match data {
                                    // Is number with same value -> still matches
                                    DataType::Number(nr) if rnr == nr => {}
                                    // Otherwise no match
                                    _ => return None,
                                }
                            }
                        }
                    }
            }
        // Now compare arithmetic subgoals
        for sg in &self.arithmetic_subgoals {
                    // Value of LHS
                    let lhs = match &sg.lhs {
                        Argument::Atom(_) => panic!("Atom in arithmetic subgoal!"),
                        Argument::Variable(var) => match bindings.get(var) {
                            Some(DataType::Number(nr)) => nr,
                            Some(DataType::Atom(_)) => {
                                panic!("Atom bound to variable in arithmetic subgoal!")
                            }
                            None => return None,
                        },
                        Argument::Number(nr) => *nr,
                    };
                    // Value of RHS
                    let rhs = match &sg.rhs {
                        Argument::Atom(_) => panic!("Atom in arithmetic subgoal!"),
                        Argument::Variable(var) => match bindings.get(var) {
                            Some(DataType::Number(nr)) => nr,
                            Some(DataType::Atom(_)) => {
                                panic!("Atom bound to variable in arithmetic subgoal!")
                            }
                            None => return None,
                        },
                        Argument::Number(nr) => *nr,
                    };
                    match sg.operator {
                        ComparisonOperator::Lt => {
                            if lhs < rhs {
                            } else {
                                return None;
                            }
                        }
                        ComparisonOperator::Lte => {
                            if lhs <= rhs {
                            } else {
                                return None;
                            }
                        }
                        ComparisonOperator::Gt => {
                            if lhs > rhs {
                            } else {
                                return None;
                            }
                        }
                        ComparisonOperator::Gte => {
                            if lhs >= rhs {
                            } else {
                                return None;
                            }
                        }
                    }
                }
        Some(bindings)
    }

    pub fn apply(&self, assertions: &[Rc<Assertion<T>>]) -> Option<Assertion<T>> {
        self.is_match(assertions)
            .map(|bindings| bindings.fill_in(self, assertions))
    }
}

impl<T: Data> ReasonerRule<String, T> {
    pub fn from_rule(rule: &Rule<String, T>) -> Self {
        ReasonerRule::new(&rule.head, &rule.subgoals)
    }
}

#[derive(Clone)]
pub struct Ruleset<Var: Data, T: Data> {
    pub rules: Vec<ReasonerRule<Var, T>>,
}


impl<Var: Data, T: Data> Ruleset<Var, T> {
    pub fn iter(&self) -> impl Iterator<Item = &ReasonerRule<Var, T>> {
        self.rules.iter()
    }
}

impl<Var: Data, T: Data> IntoIterator for Ruleset<Var, T> {
    type Item = ReasonerRule<Var, T>;
    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.rules.into_iter()
    }
}


impl<Var: Data, T: Data> Default for Ruleset<Var, T> {
    fn default() -> Self {
        Self {rules: Vec::default()}
    }
}

pub enum QueryResult<Var: Data> {
    True,
    False,
    Set(Vec<Var>),
}

pub struct Reasoner<Var: Data + Hash, T: Data + Hash> {
    rules: Ruleset<Var, T>,
    to_add: Vec<Assertion<T>>,
    assertions: AssertionSet<T>,
}

impl<Var: Data + Hash, T: Data + Hash> Default for Reasoner<Var, T> {
    fn default() -> Self {
        Self {
            rules: Ruleset::default(),
            to_add: Vec::default(),
            assertions: AssertionSet::default(),
        }
    }
}

impl<Var: Data + Hash, T: Data + Hash> Reasoner<Var, T> {
    pub fn add_fact(&mut self, data: Assertion<T>) {
        self.assertions.insert(data.predicate.clone(), data);
    }

    pub fn add_assertion_set(&mut self, data: Vec<Assertion<T>>) {
        for a in data {
            self.add_fact(a);
        }
    }

    pub fn add_rule(&mut self, rule: ReasonerRule<Var, T>) {
        self.rules.rules.push(rule)
    }

    pub fn add_ruleset(&mut self, rules: Ruleset<Var, T>) {
        self.rules.rules.extend(rules);
    }

    fn combine_buckets(
        &self,
        combinations: &[Vec<Rc<Assertion<T>>>],
        bucket: Option<&Vec<Rc<Assertion<T>>>>,
    ) -> Option<Vec<Vec<Rc<Assertion<T>>>>> {
        //todo: MAKE LAZY!
        match bucket {
            None => None,
            Some(b) => {
                if combinations.is_empty() {
                    return Some(b.clone().iter().map(|e| vec![e.clone()]).collect_vec());
                } else {
                    let cs = combinations
                        .iter()
                        .flat_map(|c| {
                            b.iter().map(move |a| {
                                let mut res = c.clone();
                                res.push(a.clone());
                                res
                            })
                        })
                        .collect_vec();
                    Some(cs)
                }
            }
        }
    }

    fn apply_rule(&self, rule: &ReasonerRule<Var, T>) -> Vec<Assertion<T>> {
        // Get all combinations
        let mut combinations = Vec::new();
        // Only check logic subgoals here
        for subgoal in &rule.logic_subgoals {
                    // Find assertions with the correct predicate
                    let bucket = self.assertions.get(&subgoal.predicate);
                    // Take all combinations with previous combinations
                    match self.combine_buckets(&combinations, bucket) {
                        None => return Vec::new(),
                        Some(v) => combinations = v,
                }
            }
        // Filter the combinations
        combinations
            .iter()
            .filter_map(|c| rule.apply(c))
            .collect_vec()
    }

    // Returns Some when finished and None otherwise
    fn reason_step(&mut self, question: &Assertion<T>) -> Result<Assertion<T>, bool> {
        //todo: we should only check things added last step
        let new_assertions = self
            .rules
            .iter()
            .flat_map(|r| self.apply_rule(r))
            .collect_vec();
        if let Some(x) = new_assertions.iter().find(|p| p == &question) {
            return Ok(x.clone());
        }
        let has_new = new_assertions
            .into_iter()
            .any(|elmt| self.assertions.insert(elmt.predicate.clone(), elmt));
        if has_new {
            Err(true)
        } else {
            Err(false)
        }
    }

    pub fn query(&mut self, query: &Question<Var, T>) -> Option<Assertion<T>> {
        let query_assertion = query.to_assertion();
        //Search in initial set
        if let Some(res) = self.assertions.find(&query.predicate, &query_assertion) {
            return Some(res.deref().clone());
        }
        //Run reasoner
        let mut reason_result = Err(true);
        while reason_result == Err(true) {
            reason_result = self.reason_step(&query_assertion);
        }
        //Result
        match reason_result {
            Ok(a) => Some(a),
            Err(false) => None,
            _ => panic!("Should never reach here!"),
        }
    }
}

/// # Iteration
#[derive(Debug, Clone)]
#[cfg_attr(test, derive(Eq, PartialEq))]
pub struct Iteration<T: Data> {
    pub assertions: Vec<Assertion<T>>,
}

/// # History
#[derive(Debug, Clone)]
pub struct History<T: Data> {
    pub iterations: Vec<Iteration<T>>,
    pub leafs: Vec<Assertion<T>>,
    //TODO: make a Circuitree feature?
    pub largest_assertion_set_size: usize,
}

impl<T: Data> History<T> {
    pub fn new(iterations: Vec<Iteration<T>>, leafs: Vec<Assertion<T>>) -> Self {
        let largest_assertion_set_size = iterations.iter().map(|i| i.assertions.len()).max().unwrap();
        Self {
            iterations,
            leafs,
            largest_assertion_set_size,
        }
    }
}


use crate::parsing::Rule;
/// # Debug
use std::fmt;
use std::ops::Deref;

impl<T: Data + Debug> fmt::Debug for DataType<T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.write_str(&match self {
            DataType::Atom(atm) => format!("{:?}", atm),
            DataType::Number(nr) => format!("{:?}", nr),
        })
    }
}

impl<T: Data + Debug> fmt::Debug for Assertion<T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.write_str(&format!(
            "{:?}{:?}:\n",
            self.predicate, self.arguments
        ))
    }
}

/// # Tests
#[cfg(test)]
mod tests {
    use crate::parsing::{Clause, Parser};
    use crate::reasoning::{Assertion, Reasoner, ReasonerRule, Iteration};

    #[test]
    fn test_in_inital_set() {
        let fact1 = match Parser::parse("hasWall(alice,wall).").unwrap() {
            Clause::Fact(f) => f,
            _ => panic!("Statement not parsed as a fact"),
        };
        let query1 = match Parser::parse("?- hasWall(alice,wall).").unwrap() {
            Clause::Question(q) => q,
            _ => panic!("Statement not parsed as a question"),
        };
        let fact2 = match Parser::parse("isLie(cake).").unwrap() {
            Clause::Fact(f) => f,
            _ => panic!("Statement not parsed as a fact"),
        };
        let query2 = match Parser::parse("?- isLie(cake).").unwrap() {
            Clause::Question(q) => q,
            _ => panic!("Statement not parsed as a question"),
        };

        let mut rea: Reasoner<String, String> = Reasoner::default();
        rea.add_fact(Assertion::from_fact(&fact1));
        rea.add_fact(Assertion::from_fact(&fact2));

        let res1 = rea.query(&query1);
        assert!(res1.is_some());
        let res2 = rea.query(&query2);
        assert!(res2.is_some());

        let not_in = match Parser::parse("?- not(in).").unwrap() {
            Clause::Question(q) => q,
            _ => panic!("Statement not parsed as a question"),
        };
        let res_no = rea.query(&not_in);
        assert!(res_no.is_none())
    }

    #[test]
    fn test_rule_minimal() {
        let hw = match Parser::parse("hasWall(alice, wall).").unwrap() {
            Clause::Fact(f) => f,
            _ => panic!("Statement not parsed as a fact"),
        };
        let hf = match Parser::parse("hasFriend(alice, bob).").unwrap() {
            Clause::Fact(f) => f,
            _ => panic!("Statement not parsed as a fact"),
        };

        let rule = match Parser::parse("canWrite(B, W) :- hasWall(A, W), hasFriend(A, B).").unwrap()
        {
            Clause::Rule(r) => r,
            _ => panic!("Statement not parsed as a rule"),
        };
        let mut rea = Reasoner::default();
        rea.add_rule(ReasonerRule::from_rule(&rule));
        rea.add_fact(Assertion::from_fact(&hw));
        rea.add_fact(Assertion::from_fact(&hf));

        let question = match Parser::parse("?- canWrite(bob, wall).").unwrap() {
            Clause::Question(q) => q,
            _ => panic!("Statement not parsed as a question"),
        };
        let res = rea.query(&question);
        assert!(res.is_some());

        let query_no = match Parser::parse("?- canWrite(alice, wall).").unwrap() {
            Clause::Question(q) => q,
            _ => panic!("Statement not parsed as a question"),
        };
        let res_no = rea.query(&query_no);
        assert!(res_no.is_none())
    }

    #[test]
    /// Test whether atoms are reasoned about as if they were variables (they shouldn't)
    fn test_rule_minimal_without_variables() {
        let hw = match Parser::parse("hasWall(alice, wall).").unwrap() {
            Clause::Fact(f) => f,
            _ => panic!("Statement not parsed as a fact"),
        };
        let hf = match Parser::parse("hasFriend(alice, bob).").unwrap() {
            Clause::Fact(f) => f,
            _ => panic!("Statement not parsed as a fact"),
        };
        //Rule doesn't contain Variables, just atoms
        let rule = match Parser::parse("canWrite(b, w) :- hasWall(a, w), hasFriend(a, b).").unwrap()
        {
            Clause::Rule(r) => r,
            _ => panic!("Statement not parsed as a rule"),
        };
        let mut rea = Reasoner::default();
        rea.add_rule(ReasonerRule::from_rule(&rule));
        rea.add_fact(Assertion::from_fact(&hw));
        rea.add_fact(Assertion::from_fact(&hf));
        // Test for erroneous reasoning
        let question = match Parser::parse("?- canWrite(bob, wall).").unwrap() {
            Clause::Question(q) => q,
            _ => panic!("Statement not parsed as a question"),
        };
        let res = rea.query(&question);
        assert!(res.is_none());

        // A question that should return false, even when atoms are passed as variables
        let query_no = match Parser::parse("?- canWrite(alice, wall).").unwrap() {
            Clause::Question(q) => q,
            _ => panic!("Statement not parsed as a question"),
        };
        let res_no = rea.query(&query_no);
        assert!(res_no.is_none());

        // Let's add the rule correctly, with Variables this time
        let var_rule =
            match Parser::parse("canWrite(B, W) :- hasWall(A, W), hasFriend(A, B).").unwrap() {
                Clause::Rule(r) => r,
                _ => panic!("Statement not parsed as a rule"),
            };
        rea.add_rule(ReasonerRule::from_rule(&var_rule));
        //Let's try the query again
        let question = match Parser::parse("?- canWrite(bob, wall).").unwrap() {
            Clause::Question(q) => q,
            _ => panic!("Statement not parsed as a question"),
        };
        let res = rea.query(&question);
        assert!(res.is_some());
    }
    #[test]
    fn test_history() {
        let f1 = match Parser::parse("a(name).").unwrap() {
            Clause::Fact(f) => f,
            _ => panic!("Statement not parsed as a fact"),
        };
        let f2 = match Parser::parse("b(name).").unwrap() {
            Clause::Fact(f) => f,
            _ => panic!("Statement not parsed as a fact"),
        };
        let f3 = match Parser::parse("d(name).").unwrap() {
            Clause::Fact(f) => f,
            _ => panic!("Statement not parsed as a fact"),
        };
        let fx = match Parser::parse("obsolote(name).").unwrap() {
            Clause::Fact(f) => f,
            _ => panic!("Statement not parsed as a fact"),
        };
        let rule1 = match Parser::parse("c(name) :- a(name), b(name).").unwrap()
        {
            Clause::Rule(r) => r,
            _ => panic!("Statement not parsed as a rule"),
        };
        let rule2 = match Parser::parse("e(name) :- c(name), d(name).").unwrap()
        {
            Clause::Rule(r) => r,
            _ => panic!("Statement not parsed as a rule"),
        };

        let mut rea = Reasoner::default();
        rea.add_rule(ReasonerRule::from_rule(&rule1));
        rea.add_rule(ReasonerRule::from_rule(&rule2));
        rea.add_fact(Assertion::from_fact(&f1));
        rea.add_fact(Assertion::from_fact(&f2));
        rea.add_fact(Assertion::from_fact(&f3));
        rea.add_fact(Assertion::from_fact(&fx));

        let question = match Parser::parse("?- e(name).").unwrap() {
            Clause::Question(q) => q,
            _ => panic!("statement not parsed as a question"),
        };
        let res = rea.query(&question);
        assert!(res.is_some());

        let c_fact = match Parser::parse("c(name).").unwrap() {
            Clause::Fact(f) => f,
            _ => panic!("statement not parsed as a fact"),
        };
        let query_fact = match Parser::parse("e(name).").unwrap() {
            Clause::Fact(f) => f,
            _ => panic!("statement not parsed as a fact"),
        };

        let history = res.unwrap().get_history();

        let iter0_sol = Iteration{assertions: vec![Assertion::from_fact(&f1), Assertion::from_fact(&f2)]};
        let iter1_sol = Iteration{assertions: vec![Assertion::from_fact(&c_fact), Assertion::from_fact(&f3)]};
        let iter2_sol = Iteration{assertions: vec![Assertion::from_fact(&query_fact)]};
        let leafs_sol = vec![Assertion::from_fact(&f3), Assertion::from_fact(&f1), Assertion::from_fact(&f2)];

        assert_eq!(history.iterations, vec!{iter0_sol, iter1_sol, iter2_sol});
        assert_eq!(history.leafs, leafs_sol);

    }

    #[test]
    fn test_basic_arithmetic() {
        let rule = match Parser::parse("over18(A) :- hasAge(A, Age), Age >= 18.").unwrap() {
            Clause::Rule(r) => r,
            _ => panic!("Statement not parsed as a rule"),
        };
        let alice_over_18 = match Parser::parse("hasAge(alice, 42).").unwrap() {
            Clause::Fact(f) => f,
            _ => panic!("Statement not parsed as a fact"),
        };
        let bob_under_18 = match Parser::parse("hasAge(bob, 13).").unwrap() {
            Clause::Fact(f) => f,
            _ => panic!("Statement not parsed as a fact"),
        };
        let mut rea = Reasoner::default();
        rea.add_rule(ReasonerRule::from_rule(&rule));
        rea.add_fact(
            Assertion::from_fact(&alice_over_18),
        );
        rea.add_fact(
            Assertion::from_fact(&bob_under_18),
        );

        let question_true = match Parser::parse("?- over18(alice).").unwrap() {
            Clause::Question(q) => q,
            _ => panic!("statement not parsed as a question"),
        };
        let question_false = match Parser::parse("?- over18(bob).").unwrap() {
            Clause::Question(q) => q,
            _ => panic!("statement not parsed as a question"),
        };

        let res = rea.query(&question_true);
        assert!(res.is_some());
        let res = rea.query(&question_false);
        assert!(res.is_none());
        //todo test a bit more
    }
}
